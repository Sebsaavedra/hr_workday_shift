# -*- coding: utf-8 -*-

from odoo.exceptions import Warning
from odoo import models, fields, api, exceptions, _
#from odoo import models, fields, api, _


# definir clase
class HrJustification(models.Model):
    _name = 'hr.justification'
    _description = 'Gestión de minutos ausentes (retrasos y salidas tempranas) y ausencias'
    
    non_attendanced_day = fields.Boolean(string = 'Faltó ese día?',default=False,
        track_visibility='onchange', readonly=True)
    justified_minutes = fields.Integer(string="Minutos de Justificación", default=0)
    comment = fields.Char(string="Comentario de Justificación")
    date = fields.Date(string = "Cuándo",track_visibility='onchange', required = True)
    employee_id = fields.Many2one('hr.employee', string="Empleado asociado")
    justified_day = fields.Boolean(string = 'Se justifica esa inasistencia?', default=False,
        track_visibility='onchange')
    n_non_attendanced_day = fields.Integer(string="Inasistencia (int)")


# Agregar a empleado los ids
class HrEmployeeInherited(models.Model):
    _inherit = 'hr.employee'
    medical_leave_ids = fields.One2many('hr.medical.leave', 'employee_id', string="Licencias médicas")
    hr_workday_shift_ids = fields.One2many('hr.shift.schedule', 'hr_employee_id', string="Turnos")
    hr_workday_movement = fields.One2many('hr.workday.movement', 'hr_employee_id', string="Turnos")
    justification_ids = fields.One2many('hr.justification', 'employee_id', string="Ausencias justificadas") # NUEVO




class HrMedicalLeave(models.Model):
    _name = 'hr.medical.leave'
    _description = 'Gestión de licencias médicas'


    comentario = fields.Char(string = "Comentario", track_visibility='onchange')

    date_from = fields.Date(string = "Desde cuándo",track_visibility='onchange', required = True)
    date_to = fields.Date(string = "Hasta cuándo",track_visibility='onchange', required = True)

    status = fields.Selection(   [  ('NEW','Nueva'),
                                    ('SUBMITED','Procesada en Isapre'),
                                    ('REJECTED','Rechazada por Isapre'),
                                    ('PAYED','Pagada por Isapre a empleador'),
                                    ('RETURNED','Cobrada por rechazo'),
                                    ],
                                string = 'Estado',
                                track_visibility='onchange',
                                default='NEW'
                            )

    monto = fields.Integer(string='Monto a recuperar')
    employee_id = fields.Many2one('hr.employee', string="Empleado asociado")

    # restriccion de fecha de inicio <= fecha de fin
    @api.constrains('date_from', 'date_to')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.date_from > r.date_to:
                raise exceptions.ValidationError("La fecha de inicio debe ser anterior a la fecha de fin.")




class HrInheritedResourceCalendar(models.Model):
    _inherit = 'resource.calendar'

    color = fields.Integer(string='Color')
    workday_schedule_ids = fields.One2many('hr.workday.schedule', 'resource_calendar_id', string="Jornada de trabajo")



class HrEmployeeContract(models.Model):
    _inherit = 'hr.contract'

    workday_schedule_ids = fields.One2many('hr.workday.schedule', 'hr_contract_id', string="Jornada")
    shift_schedule_ids = fields.One2many('hr.shift.schedule', 'hr_contract_id', string="Turnos o horas extraordinarias")


    workday_movement_ids = fields.One2many('hr.workday.movement', 'hr_contract_id', string="Procesamiento de jornadas")


    # @api.multi
    # def name_get(self):
    #     result = []
    #     for record in self:
    #         record_name = result.append((record.id,record.name + " // F: " + record.employee_id.name+ " // D:" + record.department_id.name))
    #     return result


class HrWorkdaySchedule(models.Model):
    _name = 'hr.workday.schedule'
    _description =  'Planificación de jornadas'

    start_date = fields.Date(string="Fecha desde", required=True)
    end_date = fields.Date(string="Fecha hasta", required=True)


    # restriccion de fecha de inicio <= fecha de fin
    @api.constrains('start_date', 'end_date')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.start_date > r.end_date:
                raise exceptions.ValidationError("La fecha de inicio debe ser anterior a la fecha de fin.")



    hr_contract_id = fields.Many2one('hr.contract', string="Contrato de trabajo asociado")
    resource_calendar_id = fields.Many2one('resource.calendar', string="Jornada de trabajo", required=True)

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            record_name = result.append(( record.id,record.resource_calendar_id.name + " (" + record.start_date.strftime("%d/%m/%Y") + " - " + record.end_date.strftime("%d/%m/%Y") + ")" ))
        return result



class HrShiftSchedule(models.Model):
    _name = 'hr.shift.schedule'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Tunos y horas extraordinarias'

    start = fields.Datetime(string = "Desde cuándo",track_visibility='onchange', required = True)
    end = fields.Datetime(string = "Hasta cuándo",track_visibility='onchange', required = True)

    # restriccion de fecha de inicio <= fecha de fin
    @api.constrains('start', 'end')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.start > r.end:
                raise exceptions.ValidationError("El inicio debe ser anterior al fin.")


    description = fields.Char(string = "Description", track_visibility='onchange')

    state = fields.Selection(   [  ('REQUEST','Solicitado'),
                                    ('APPROVED','Aprobado'),
                                    ('REQUEST_REVERSAL','Solicitado revertir')],
                                string = 'Estado',
                                track_visibility='onchange',
                                default='REQUEST',
                                states={'APPROVED':[('readonly',True)]}
                            )
    # field_name = fields.field_type(string='Field name', store=True, readonly=False, )
    type = fields.Selection([   ('SHIFT','Turno'),
                                ('EXTSHIFT','Hora extraordinaria')], string = 'Tipo', track_visibility='onchange')

    hr_employee_id = fields.Many2one('hr.employee', string="Empleado asociado")
    hr_contract_id = fields.Many2one('hr.contract', domain="[('employee_id', '=', hr_employee_id)]", string="Contrato de trabajo asociado")


    @api.onchange('hr_employee_id')
    def on_change_hr_employee_id(self):
        # import logging
        # _logger = logging.getLogger(__name__)
        # _logger.debug("hola" + str(self.hr_contract_id))
        self.hr_contract_id = None

    @api.one
    def name_get(self):
        res = (self.id,str(self.start) + " - " + str(self.end) + " - " + self.hr_contract_id.name )
        return res
    #     	@api.model
    #
	# def name_search(self, name, args=None, operator='ilike', limit=):
	# 	args = args or []
    #
	# 	if name:
	# 	# Be sure name_search is symetric to name_get
	# 		name = name.split(' / ')[-1]
	# 		args = [('name', operator, name)] + args
	# 	locs = self.search(args, limit=limit)
	# 	return locs.name_get()

    @api.one
    def btn_submit_shift_APPROVE(self):
        self.write({'state':'APPROVED'})
    @api.one
    def btn_submit_shift_HR_REQUEST_REVERSAL(self):
        self.sudo().write({'state':'REQUEST_REVERSAL'})
    @api.one
    def btn_submit_shift_REVERSE(self):
        self.sudo().write({'state':'REQUEST'})

class HrInheritedDepartment(models.Model):
    _inherit = 'hr.department'

    hr_workday_process_ids = fields.One2many('hr.workday.process', 'hr_department_id', string="Procesamiento de jornada de trabajo")

class HrWorkdayProcess(models.Model):
    _name = 'hr.workday.process'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Procesamiento de jornadas + turnos + horas extraordinarias, por periodo y establecimiento'

    month = fields.Selection(   [   (1,'Enero'),
                                    (2,'Febrero'),
                                    (3,'Marzo'),
                                    (4,'Abril'),
                                    (5,'Mayo'),
                                    (6,'Junio'),
                                    (7,'Julio'),
                                    (8,'Agosto'),
                                    (9,'Septiembre'),
                                    (10,'Octubre'),
                                    (11,'Noviembre'),
                                    (12,'Diciembre')
                                ],
                                string = 'Mes periodo',
                                track_visibility='onchange' , required = True)

    year = fields.Selection(    [   (2019,'2019'),
                                    (2020,'2020'),
                                    (2021,'2021'),
                                    (2022,'2022'),
                                    (2023,'2023'),
                                    (2024,'2024')
                                ],
                                string = 'Año periodo',
                                track_visibility='onchange', required = True
                            )

    hr_department_id = fields.Many2one('hr.department', string="Departamento/Establecimiento", required = True)
    hr_workday_movement_ids = fields.One2many('hr.workday.movement', 'hr_workday_process_id', string="Registro de jornada", track_visibility='onchange')
    tz_offset = fields.Selection(   [   (-2,'-2'),
                                        (-1,'-1'),
                                        (0,'+0'),
                                        (1,'+1'),
                                        (2,'+2')
                                    ],
                                    string = 'Corrección de hora',
                                    track_visibility='onchange' , required = True)

    @api.one
    def btn_submit_DELETE_MOVEMENTS(self,cr):
        workday_query = "delete from hr_workday_movement where hr_workday_process_id = " + str(self.id)
        self.env.cr.execute(workday_query)
        self.env.cr.commit()

    @api.one
    def btn_submit_REPROCESS(self,cr):
        HrWorkdayProcess.btn_submit_DELETE_MOVEMENTS(self,cr)
        HrWorkdayProcess.btn_submit_PROCESS(self,cr)

    @api.one
    def btn_submit_PROCESS(self,cr):
        import datetime
        import json
        import logging
        _logger = logging.getLogger(__name__)


        import datetime, calendar
        year = self.year
        month = self.month
        num_days = calendar.monthrange(year, month)[1]

        department_id = self.hr_department_id.id

        periods = {}


        # for each day of the processed month
        import pytz
        # import datetime

        # now = datetime.datetime.now()
        # offset = tz.utcoffset(now)-datetime.timedelta(hours=self.tz_offset)
        # _logger.debug("offset--" + str(offset))

        tz_utc = pytz.timezone('UTC')

        #########
        # Main loop of queries for each day of month.
        for day in range(1, num_days+1):
            # utc_time = datetime.datetime(year, month, day, 0, 0, 0, 0, tzinfo=tz_utc)
            # user_time = datetime.datetime(year, month, day, 0, 0, 0, 0)
            # offset =  ((datetime.datetime.fromtimestamp(user_time) - datetime.datetime.utcfromtimestamp(user_time)).total_seconds())
            # _logger.debug("offset--" + str(offset))
            # offset = tz.utcoffset(now)-datetime.timedelta(hours=self.tz_offset)



            periods[day] = {}
            computed_day = datetime.date(year, month, day)

            weekday_string = computed_day.strftime("%A")
            weekday_number = computed_day.weekday()
            date_string = computed_day.strftime("%Y-%m-%d")


            # Create a single array
            temp_periods = []

            ##########
            # WORKDAYS
            workday_query = """
                            select      w.id,
                                        a.hour_from,
                                        a.hour_to,
                                        c.id,
                                        r.tz,
                                        c.employee_id
                            from        hr_workday_schedule w,
                                        hr_contract c,
                                        resource_calendar r,
                                        resource_calendar_attendance a
                            where       a.calendar_id = r.id AND
                                        r.id = w.resource_calendar_id AND
                                        c.id = w.hr_contract_id AND
                                        c.department_id = """ + str(department_id) + """ AND
                                        a.dayofweek = '""" + str(weekday_number) + """' AND
                                        w.start_date <= '""" + date_string + """' AND
                                        w.end_date >= '""" + date_string + """'
                            """

            # print(workday_query)
            # Array with all posible workday in the currente date. Yes, it could be more than one
            # form example if you have two 4 hours shifts, one in the morning and another at evening.
            #
            # print(workday_query)
            # break

            self.env.cr.execute(workday_query)
            workday_rows = self.env.cr.fetchall()
            # import pytz
            # _logger.debug(pytz.__version__)

            if len(workday_rows)>0:
                for wr in workday_rows:
                    hours_from = int(wr[1])
                    minutes_from = int((wr[1]*60) % 60)
                    # print(hours_from)
                    # print(minutes_from)
                    # datetime(year, month, day, hour, minute, second, microsecond)

                    # _logger.debug("--_____" + str(wr[4]))
                    tz = pytz.timezone(wr[4])
                    tz_utc = pytz.timezone('UTC')

                    # print(tz)
                    #_logger.debug(str(t_period['start']))
                    # Build workday datetime
                    # _logger.debug(">>" + str(hours_from))
                    s1 = datetime.datetime(year, month, day, hours_from, minutes_from, 0, 0)
                    info = s1.tzinfo
                    original = s1
                    s1 = tz.localize(s1, is_dst=True)
                    localized = s1

                    info = s1.tzinfo
                    s1 = s1.astimezone(tz_utc)
                    as_time = s1
                    # _logger.debug('from: '+ str(original) + ' (info: ' + str(info) +') >> ' + str(localized)+ '>> ' + str(as_time) )
                    # s1 = s1-tz.utcoffset(s1, is_dst=False)
                    # s1 = s1-offset
                    # s1 = s1-UTC_OFFSET_TIMEDELTA

                    # print(s1)

                    hours_to = int(wr[2])
                    minutes_to = int((wr[2]*60) % 60)
                    e1 = datetime.datetime(year, month, day, hours_to, minutes_to, 0, 0)
                    original = e1
                    e1 = tz.localize(e1)
                    localized = e1
                    e1 = e1.astimezone(tz_utc)
                    as_time = e1
                    # _logger.debug('--- to: ' + str(original) + '>> ' + str(localized)+ '>> ' + str(as_time) )

                    temp_periods.append({'id': wr[0], 'start': s1, 'end': e1, 'type': "workday", 'id_contract':wr[3], 'id_employee': wr[5]})


            ###########
            # SHIFTS

            shift_query =   """
                            select      s.id,
                                        s.start,
                                        s.end,
                                        c.id,
                                        c.employee_id
                            from        hr_shift_schedule s,
                                        hr_contract c
                            where       c.id = s.hr_contract_id AND
                                        c.department_id = """ + str(department_id) + """ AND
                                        s.start >= '""" + date_string + " 00:00:00" """' AND
                                        s.start <= '""" + date_string + " 23:59:59"  + """'
                            """

            # print(shift_query)
            # Array with all shifts for the current date
            self.env.cr.execute(shift_query)
            shift_rows = self.env.cr.fetchall()


            if len(shift_rows)>0:
                for sr in shift_rows:


                    shift_start=sr[1].astimezone(tz_utc)
                    # shift_start = tz_utc.localize(sr[1])
                    shift_end=sr[2].astimezone(tz_utc)
                    # shift_end = tz_utc.localize(sr[3])
                    # _logger.debug("+++ " + str(shift_start))

                    temp_periods.append({'id': sr[0], 'start': shift_start, 'end': shift_end, 'type': "shift", 'id_contract': sr[3], 'id_employee': sr[4]})


            # _logger.debug("                 _____")
            ###################
            # Build all dictionaries
            for temp_period in temp_periods:
                if temp_period['id_contract'] not in periods[day].keys():
                    periods[day][temp_period['id_contract']] = {
                                                                "info": {
                                                                    "id_employee": None
                                                                },
                                                                "normal":[],
                                                                "join":[],
                                                                "base_join":[],
                                                                "medical_leaves":[],
                                                                "justifications":[],
                                                                "leaves":[]
                                                                }

                periods[day][temp_period['id_contract']]['normal'].append(temp_period)
                periods[day][temp_period['id_contract']]['info']['id_employee'] = temp_period['id_employee']
                periods[day][temp_period['id_contract']]['info']['department_id'] = department_id
                periods[day][temp_period['id_contract']]['info']['identification_id'] = self.env['hr.contract'].search([('id','=',temp_period['id_contract'])], limit=1).employee_id.identification_id

            # print(json.dumps(periods, indent=4, sort_keys=True))

        # print(periods)

        # END OF LOOP DAYS

        #############
        # Sort ASC of contract's period
        # for day, period in periods.items():
        #     if len(period.items())>0:
        #         for id_contract, contract in period.items():
        #             i = 0
        #             painted = []
        #             contract_periods = contract['normal']


        def sort_periods(contract_periods):
            i=0
            result_array = []
            painted = []
            while i < len(contract_periods):
                smallest = datetime.datetime(2100,12,12,0,0,0,0).astimezone(tz_utc)
                smallest_key = None

                for array_index, t_period in enumerate(contract_periods):
                    if array_index not in painted:
                        if t_period["start"] <= smallest:
                            smallest = t_period["start"]
                            smallest_key = array_index

                painted.append(smallest_key)
                result_array.append(contract_periods[smallest_key])
                i = i+1

            return(result_array)

        ##########
        # Recursive Function that transform an array of periods into continuous periods, if they intersect.
        # It builds a tree with all prosibilities.
        #   contract_perdiods: array of periods, with start and end
        #   start_index: current start index of a continuous period
        #   end_index: current end index of a continuou period
        #   candidate_index: current index to be analyzed
        #   banned: indexes already included in a continuous period
        def recursive_join_periods(contract_periods, start_index, end_index, candidate_index, banned):
            # sc =  scorelevel(candidate_index)
            # print(sc + '> start_index: ' +str(start_index) )
            # print(sc + '> end_index: ' + str(end_index))
            # print(sc + '> candidate_index: ' + str(candidate_index))
            # print(sc + '> banned: ' + str(banned))

            if len(contract_periods) > candidate_index:
                # print('---- Entra ['+str(len(contract_periods))+','+str(current_end_index+1)+']')
                # print('------ c_end     ' + str(contract_periods[current_end_index]['end']))
                # print('------ c+1_end   ' + str(contract_periods[current_end_index+1]['end']))
                # print('------ c_end     ' + str(contract_periods[current_end_index]['end']))
                # print('------ c+1_start ' + str(contract_periods[current_end_index+1]['start']))
                # if current and next intersect, then next is banned.
                # I start with the NO intersection case
                if candidate_index not in banned:
                    if (contract_periods[start_index]['start'] >= contract_periods[candidate_index]['end'] and contract_periods[start_index]['start'] >= contract_periods[candidate_index]['start']):
                        # print(sc + ' >> case1 ')
                        banned.append(candidate_index)
                        return(recursive_join_periods(contract_periods, level_index, end_index, candidate_index+1,  banned ))
                    elif (contract_periods[start_index]['start'] <= contract_periods[candidate_index]['start'] and contract_periods[end_index]['end'] >= contract_periods[candidate_index]['end']):
                        # print(sc + ' >> case2 ')
                        banned.append(candidate_index)
                        return(recursive_join_periods(contract_periods, start_index, end_index, candidate_index+1, banned ))
                    elif (contract_periods[end_index]['end'] >= contract_periods[candidate_index]['start'] and contract_periods[end_index]['end'] <= contract_periods[candidate_index]['end']):
                        # print(sc + ' >> case3 ')
                        banned.append(candidate_index)
                        return(recursive_join_periods(contract_periods, start_index, candidate_index, candidate_index+1, banned ))
                    else:
                        # print(sc + ' >> case4 ')
                        return [start_index, end_index, banned]
                else:
                    # print(sc + ' >> case5 ')
                    return(recursive_join_periods(contract_periods, start_index, end_index, candidate_index+1, banned ))
            else:
                # print(sc + ' >> case6 ')
                return [start_index, end_index, banned]

        def join_periods(contract_periods):

            banned = [0]
            result_array = []
            ok = True
            current_start = 0
            if len(contract_periods)>0:
                print(contract_periods)
                while ok:
                    nperiod = recursive_join_periods(
                        contract_periods,
                        current_start,
                        current_start,
                        current_start+1,
                        banned
                    )
                    banned = nperiod[2]
                    print('nperiod result: '+str(nperiod))
                    result_array.append({'start': contract_periods[nperiod[0]]['start'],'end': contract_periods[nperiod[1]]['end']})
                    ok2 = True
                    while ok2:
                        current_start = nperiod[1] + 1
                        if current_start not in banned:
                            ok2 = False
                    if len(contract_periods) > current_start:
                        ok = True
                    else:
                        ok = False
            return(result_array)



        def recursive_cut_periods(join_period, cuts_array, current_cut, temp):
            _logger.debug("\n\n+++ ENTRA al recursive con este CUT: " + str(cuts_array) + "\n+++ ENTRA al recursive con este join: " + str(join_period) + "\n\n")
            if len(join_period)>0:
                new_periods = []
                if current_cut < len(cuts_array):
                    _logger.debug("\n\n ------------------ENTRA a los casos")
                    if cuts_array[current_cut]['end'] > join_period['start'] and  cuts_array[current_cut]['end'] < join_period['end'] and  cuts_array[current_cut]['start'] <= join_period['start']:
                        new_periods.append({'start': cuts_array[current_cut]['end'], 'end': join_period['end']})

                        _logger.debug("\n\n\n+++ CUTS: ENTRA 1")

                    elif cuts_array[current_cut]['start'] < join_period['end'] and  cuts_array[current_cut]['start'] > join_period['start'] and cuts_array[current_cut]['end'] >= join_period['end']:
                        new_periods.append({'start': join_period['start'], 'end': cuts_array[current_cut]['start']})

                        _logger.debug("\n\n\n+++ CUTS: ENTRA 2")

                    elif cuts_array[current_cut]['start'] > join_period['start'] and  cuts_array[current_cut]['end'] < join_period['end']:
                        new_periods.append({'start': join_period['start'], 'end': cuts_array[current_cut]['start']})
                        new_periods.append({'start': cuts_array[current_cut]['end'], 'end': join_period['end']})

                        _logger.debug("\n\n\n+++ CUTS: ENTRA 3")

                    elif cuts_array[current_cut]['start'] <= join_period['start'] and  cuts_array[current_cut]['end'] >= join_period['end']:


                        _logger.debug("\n\n\n+++ CUTS: ENTRA 4")

                    else:
                        new_periods.append({'start': join_period['start'], 'end': join_period['end']})
                        _logger.debug("\n\n\n+++ CUTS: ENTRA 5")

                    current_cut = current_cut + 1

                    for new_period in new_periods:
                        temp = temp + recursive_cut_periods(new_period, cuts_array, current_cut, temp)

                    return(temp)

                    # return temp

            _logger.debug("\n\n+++ SALE del recursive con este este join: " + str(join_period) + "\n\n")
            return([join_period])


        def cut_periods(join_array, cuts_array):
            cuts = join_periods(sort_periods(cuts_array))
            new_join = []

            for join_period in join_array:
                new_join = new_join + recursive_cut_periods(join_period, cuts, 0, new_join)

            return new_join


        #########################
        # END FO COMMON FUNCTIONS

        ############################################################
        # Union of periods, using the above recursive function
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    current_start = 0
                    ok = True


                    join_array = join_periods(sort_periods(contract['normal']))
                    if len(join_array)>0:
                        periods[day][id_contract]['join'] = join_array
                        periods[day][id_contract]['base_join'] = join_array.copy()

        ################
        # LEAVES

        # Read
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    contract_periods = contract['join']
                    contract['leaves'] = []
                    for array_index, t_period in enumerate(contract_periods):
                        # employee_id = self.env['hr.contract'].search([('id','=',id_contract)], limit=1).employee_id.id
                        # It would have been more efficient to include the following variables before, but it's too late.

                        # First variable: leaves


                        current_date = datetime.datetime(year, month, day)
                        lt_start = t_period['start'].strftime("%Y-%m-%d %H:%M:%S")
                        lt_end = t_period['end'].strftime("%Y-%m-%d %H:%M:%S")

                        leave_query = """
                                select      id,
                                            date_from,
                                            date_to
                                from        hr_leave
                                where       employee_id = """ + str(contract['info']['id_employee']) + """ AND
                                            NOT (
                                                (date_from      <  '"""   + lt_start + """'      AND     date_to    <   '""" + lt_end + """') OR
                                                (date_from      >  '"""   + lt_start + """'      AND     date_to    >   '""" + lt_end + """')
                                            ) AND
                                            state = 'validate'
                                """
                        #
                        _logger.debug("                 _____")
                        _logger.debug(leave_query)
                        self.env.cr.execute(leave_query)
                        leave_rows = self.env.cr.fetchall()
                        if len(leave_rows)>0:
                            for lr in leave_rows:
                                contract['leaves'].append({'start': lr[1].astimezone(tz_utc), 'end': lr[2].astimezone(tz_utc), 'id': lr[0]})

        # Process
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    if len(contract['leaves'])>0 and len(contract['join'])>0:

                        temp = cut_periods(contract['join'], contract['leaves'])
                        _logger.debug("\n\n\nOld Join: " + str(contract['join']) + "\n")
                        _logger.debug("\n\n\nNew Join: " + str(temp) + "\n")

                        # if len(temp)>0:
                        contract['join'].clear()
                        contract['join'] = temp
                        _logger.debug("\n\n\nIns Join: " + str(contract['join']) + "\n")




        ################
        # JUSTIFICATIONS

        # Read
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    contract_periods = contract['join']
                    contract['justification'] = None
                    for array_index, t_period in enumerate(contract_periods):
                        # employee_id = self.env['hr.contract'].search([('id','=',id_contract)], limit=1).employee_id.id
                        # It would have been more efficient to include the following variables before, but it's too late.

                        lt_start = t_period['start'].strftime("%Y-%m-%d")
                        lt_end = t_period['end'].strftime("%Y-%m-%d")

                        # Filtro por el negativo de la condición que busco, dado que son menos casos.
                        justification_query = """
                            select      id,
                                        date,
                                        justified_minutes,
                                        comment,
                                        justified_day,
                                        n_non_attendanced_day
                            from        hr_justification
                            where       employee_id = """ + str(contract['info']['id_employee']) + """ AND
                                        NOT(
                                            (date  <   '"""    + lt_start + """'   AND     date     <   '""" + lt_end + """') OR
                                            (date  >   '"""    + lt_start + """'   AND     date     >   '""" + lt_end + """')
                                        )
                        """
                        #
                        # _logger.debug("                 _____")
                        # _logger.debug(justification_query)
                        self.env.cr.execute(justification_query)
                        justification_rows = self.env.cr.fetchall()
                        if len(justification_rows)>0:
                            for mr in justification_rows:
                                all_day_start = datetime.datetime.strptime(str(mr[1])+" 00:00:00","%Y-%m-%d %H:%M:%S").astimezone(tz_utc)
                                all_day_end = all_day_start+datetime.timedelta(days=1)
                                # all_day_end = all_day_start + datetime.timedelta(days=1)
                                contract['justifications'].append({'start': all_day_start, 
                                    'end': all_day_end, 
                                    'id': mr[0], 
                                    'justified_minutes': mr[2], 
                                    'comment': mr[3], 
                                    'justified_day': mr[4],
                                    'n_non_attendanced_day': mr[5]
                                    })
                                _logger.debug(str(contract['justifications']))

        #for day, period in periods.items():
        #    if len(period.items())>0:
        #        for id_contract, contract in period.items():
        #            if len(contract['justifications'])>0 and len(contract['join'])>0:

        #                temp = cut_periods(contract['join'], contract['justifications'])
        #                # _logger.debug("\n\n\nOld Join: " + str(contract['join']) + "\n")
        #                # _logger.debug("\n\n\nNew Join: " + str(temp) + "\n")

        #                # if len(temp)>0:
        #                contract['join'].clear()
        #                contract['join'] = temp
        #                # _logger.debug("\n\n\nIns Join: " + str(contract['join']) + "\n")

        ################
        # MEDICAL LEAVES

        # Read
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    contract_periods = contract['join']
                    contract['medical_leave'] = None
                    for array_index, t_period in enumerate(contract_periods):
                        # employee_id = self.env['hr.contract'].search([('id','=',id_contract)], limit=1).employee_id.id
                        # It would have been more efficient to include the following variables before, but it's too late.

                        lt_start = t_period['start'].strftime("%Y-%m-%d")
                        lt_end = t_period['end'].strftime("%Y-%m-%d")

                        # Filtro por el negativo de la condición que busco, dado que son menos casos.
                        medical_leave_query = """
                            select      id,
                                        date_from,
                                        date_to
                            from        hr_medical_leave
                            where       employee_id = """ + str(contract['info']['id_employee']) + """ AND
                                        NOT(
                                            (date_from  <   '"""    + lt_start + """'   AND     date_to     <   '""" + lt_end + """') OR
                                            (date_from  >   '"""    + lt_start + """'   AND     date_to     >   '""" + lt_end + """')
                                        )
                        """
                        #
                        # _logger.debug("                 _____")
                        # _logger.debug(medical_leave_query)
                        self.env.cr.execute(medical_leave_query)
                        medical_leave_rows = self.env.cr.fetchall()
                        if len(medical_leave_rows)>0:
                            for mr in medical_leave_rows:
                                all_day_start = datetime.datetime.strptime(str(mr[1])+" 00:00:00","%Y-%m-%d %H:%M:%S").astimezone(tz_utc)
                                all_day_end = datetime.datetime.strptime(str(mr[2])+" 00:00:00","%Y-%m-%d %H:%M:%S").astimezone(tz_utc)+datetime.timedelta(days=1)
                                # all_day_end = all_day_start + datetime.timedelta(days=1)
                                contract['medical_leaves'].append({'start': all_day_start, 'end': all_day_end, 'id': mr[0]})
                                _logger.debug(str(contract['medical_leaves']))

        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    if len(contract['medical_leaves'])>0 and len(contract['join'])>0:

                        temp = cut_periods(contract['join'], contract['medical_leaves'])
                        # _logger.debug("\n\n\nOld Join: " + str(contract['join']) + "\n")
                        # _logger.debug("\n\n\nNew Join: " + str(temp) + "\n")

                        # if len(temp)>0:
                        contract['join'].clear()
                        contract['join'] = temp
                        # _logger.debug("\n\n\nIns Join: " + str(contract['join']) + "\n")

        ##########
        ## CLOCK
        for day, period in periods.items():
            if len(period.items())>0:
                for id_contract, contract in period.items():
                    contract_periods = contract['join']
                    for array_index, t_period in enumerate(contract_periods):
                        # employee_id = self.env['hr.contract'].search([('id','=',id_contract)], limit=1).employee_id.id
                        # It would have been more efficient to include the following variables before, but it's too late.

                        ct_start = t_period['start'].strftime("%Y-%m-%d %H:%M:%S")
                        ct_end = t_period['end'].strftime("%Y-%m-%d %H:%M:%S")

                        ###############
                        ## 1 START

                        ## 1a) First, search for the closest start, before the expected entry #DESCOMENTAR ->
                        #clock_start_query = """
                        #    select      id,
                        #                punch,
                        #                EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp - punch)) as time_diff
                        #    from        hr_clock_punch
                        #    where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                        #                hr_department_id = """ + str(contract['info']['department_id']) + """ AND
                        #                EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp) - punch) < 60*60*4 AND
                        #                EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp) - punch) > 0
                        #    order by    time_diff asc
                        #    limit       1
                        #""" #<-
                        # 1a) First, search for the closest start, before the expected entry
                        clock_start_query = """
                            select      id,
                                        punch,
                                        EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp - punch)) as time_diff
                            from        hr_clock_punch
                            where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                                        EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp) - punch) < 60*60*4 AND
                                        EXTRACT(EPOCH FROM ('""" + ct_start + """'::timestamp) - punch) > 0
                            order by    time_diff asc
                            limit       1
                        """

                        # _logger.debug("                 _____")
                        # _logger.debug(clock_start_query)

                        self.env.cr.execute(clock_start_query)
                        clock_start_rows = self.env.cr.fetchall()
                        if len(clock_start_rows)>0:
                            t_period['closest_start_id'] = clock_start_rows[0][0]
                            t_period['closest_start'] = clock_start_rows[0][1].astimezone(tz_utc)
                        else:
                            ## 1b Then, search for the closet start, after the expected entry #DESCOMENTAR ->
                            #clock_start_query = """
                            #    select      id,
                            #                punch,
                            #                EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) as time_diff
                            #    from        hr_clock_punch
                            #    where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                            #                hr_department_id = """ + str(contract['info']['department_id']) + """ AND
                            #                EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) < 60*60*4 AND
                            #                EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) > 0
                            #    order by    time_diff asc
                            #    limit       1
                            #""" #<-

                            # 1b Then, search for the closet start, after the expected entry
                            clock_start_query = """
                                select      id,
                                            punch,
                                            EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) as time_diff
                                from        hr_clock_punch
                                where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                                            EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) < 60*60*4 AND
                                            EXTRACT(EPOCH FROM (punch - '""" + ct_start + """'::timestamp)) > 0
                                order by    time_diff asc
                                limit       1
                            """

                            # _logger.debug("                 _____")
                            # _logger.debug(clock_start_query)

                            self.env.cr.execute(clock_start_query)
                            clock_start_rows = self.env.cr.fetchall()

                            if len(clock_start_rows)>0:
                                t_period['closest_start_id'] = clock_start_rows[0][0]
                                t_period['closest_start'] = clock_start_rows[0][1].astimezone(tz_utc)
                            else:
                                t_period['closest_start_id'] = None
                                t_period['closest_start'] = None


                        ###############
                        ## 2 END

                        ## 2a) First, search for the closest start, before the expected entry #DESCOMENTAR ->
                        #clock_end_query = """
                        #    select      id,
                        #                punch,
                        #                EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) as time_diff
                        #    from        hr_clock_punch
                        #    where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                        #                hr_department_id = """ + str(contract['info']['department_id']) + """ AND
                        #                EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) < 60*60*4 AND
                        #                EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) > 0
                        #    order by    time_diff asc
                        #    limit       1
                        #""" #<-
                        # 2a) First, search for the closest start, before the expected entry #DESCOMENTAR ->
                        clock_end_query = """
                            select      id,
                                        punch,
                                        EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) as time_diff
                            from        hr_clock_punch
                            where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                                        EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) < 60*60*4 AND
                                        EXTRACT(EPOCH FROM (punch - '""" + ct_end + """'::timestamp)) > 0
                            order by    time_diff asc
                            limit       1
                        """

                        # 2b Then, search for the closet start, after the expected entry
                        self.env.cr.execute(clock_end_query)
                        clock_end_rows = self.env.cr.fetchall()
                        if len(clock_end_rows)>0:
                            t_period['closest_end_id'] = clock_end_rows[0][0]
                            t_period['closest_end'] = clock_end_rows[0][1].astimezone(tz_utc)
                        else:
                            #clock_end_query = """ #DESCOMENTAR ->
                            #    select      id,
                            #                punch,
                            #                EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) as time_diff
                            #    from        hr_clock_punch
                            #    where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                            #                hr_department_id = """ + str(contract['info']['department_id']) + """ AND
                            #                EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) < 60*60*4 AND
                            #                EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) > 0
                            #    order by    time_diff asc
                            #    limit       1
                            #""" #<-
                            clock_end_query = """
                                select      id,
                                            punch,
                                            EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) as time_diff
                                from        hr_clock_punch
                                where       identification_id = '""" + str(contract['info']['identification_id']) + """' AND
                                            EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) < 60*60*4 AND
                                            EXTRACT(EPOCH FROM ('""" + ct_end + """'::timestamp) - punch) > 0
                                order by    time_diff asc
                                limit       1
                            """


                            self.env.cr.execute(clock_end_query)
                            clock_end_rows = self.env.cr.fetchall()

                            if len(clock_end_rows)>0:
                                t_period['closest_end_id'] = clock_end_rows[0][0]
                                t_period['closest_end'] = clock_end_rows[0][1].astimezone(tz_utc)

                            else:
                                t_period['closest_end_id'] = None
                                t_period['closest_end'] = None


        ##################################################################
        # INSERT NEW MOVEMENTS
        ## And relations to workdays, shifts, leaves, medical_leaves
        # _logger.debug('start: 'str(t_period['start']) + + 'leave')

        for day, period in periods.items():
            if len(period.items())>0:

                for id_contract, contract in period.items():
                    contract_periods = contract['join']

                    if len(contract_periods)>0:
                        for array_index, t_period in enumerate(contract_periods):

                            employee_id = contract['info']['id_employee']

                            # create array of workdays and shifts used for each joint period
                            n_workday_periods = []
                            n_shift_periods = []
                            for n_array_index, n_period in enumerate(contract['normal']):
                                if n_period['type'] == 'workday':
                                    n_workday_periods.append((4,n_period['id']))
                                elif n_period['type'] == 'shift':
                                    n_shift_periods.append((4,n_period['id']))
                            # create array of leaves used for each joint period
                            l_leave_periods = []
                            for l_array_index, l_period in enumerate(contract['leaves']):
                                l_leave_periods.append((4,l_period['id']))
                            # medical leaves
                            m_leave_periods = []
                            for m_array_index, m_period in enumerate(contract['medical_leaves']):
                                m_leave_periods.append((4,m_period['id']))

                            # justifications
                            j_leave_periods = []
                            j_aux = 0
                            c = ""
                            d = False
                            nd = 0
                            for j_array_index, j_period in enumerate(contract['justifications']):
                                j_leave_periods.append((4,j_period['id']))
                                #j_aux = j_aux + j_period['justified_minutes']
                                j_aux = j_period['justified_minutes']
                                c = j_period['comment']
                                d = j_period['justified_day']
                                nd = j_period['n_non_attendanced_day']
                                

                            _logger.debug('DAY: ' + str(day) + " // period" + str(t_period))

                            ##################
                            # Difference logic
                            difference = 0
                            non_attendanced_day = False
                            n_non_attendanced_day = 0
                            # NUEVO ->
                            if t_period['closest_start'] == None and t_period['closest_end'] == None:
                                difference = 0
                                non_attendanced_day = True
                                n_non_attendanced_day = 1

                            # <- (lo siguiente era solo if)
                            elif t_period['closest_start'] == None or t_period['closest_end'] == None:
                                difference = (t_period['end'] - t_period['start']).total_seconds()/60
                            else:
                                if t_period['closest_start'] > t_period['start']:
                                    minutesDiff = (t_period['closest_start']-t_period['start']).total_seconds()/60
                                    difference = difference + minutesDiff

                                if t_period['closest_end'] < t_period['end']:
                                    minutesDiff = (t_period['end'] - t_period['closest_end']).total_seconds()/60
                                    difference = difference + minutesDiff
                            if d:
                                n_non_attendanced_day = 0
                                j_aux = 0

                            hr_workday_movement = self.env['hr.workday.movement'].create(
                                                            {
                                                                'day_of_week': t_period['start'].strftime("%A"),
                                                                'hr_workday_process_id': self.id,
                                                                'start_time': t_period['start'],
                                                                'end_time': t_period['end'],
                                                                'closest_start': t_period['closest_start'],
                                                                'closest_end': t_period['closest_end'],
                                                                'expected_check': 1,
                                                                'hr_contract_id': id_contract,
                                                                'hr_employee_id': employee_id,
                                                                'hr_department_id': department_id,
                                                                'hr_workday_schedule_ids': n_workday_periods,
                                                                'hr_shift_schedule_ids': n_shift_periods,
                                                                'hr_leave_ids': l_leave_periods,
                                                                'hr_medical_leave_ids': m_leave_periods,
                                                                'hr_justification_ids': j_leave_periods,
                                                                'difference': difference,
                                                                'non_attendanced_day': non_attendanced_day,
                                                                'n_non_attendanced_day': n_non_attendanced_day,
                                                                'justified_minutes': j_aux,
                                                                'comment': c,
                                                                'justified_day': d

                                                            })
                    elif len(contract['base_join']) > 0:
                        contract_periods = contract['base_join']
                        for array_index, t_period in enumerate(contract_periods):
                            employee_id = contract['info']['id_employee']

                            # create array of workdays and shifts used for each joint period
                            n_workday_periods = []
                            n_shift_periods = []
                            for n_array_index, n_period in enumerate(contract['normal']):
                                if n_period['type'] == 'workday':
                                    n_workday_periods.append((4,n_period['id']))
                                elif n_period['type'] == 'shift':
                                    n_shift_periods.append((4,n_period['id']))
                            # create array of leaves used for each joint period
                            l_leave_periods = []
                            for l_array_index, l_period in enumerate(contract['leaves']):
                                l_leave_periods.append((4,l_period['id']))
                            # medical leaves
                            m_leave_periods = []
                            for m_array_index, m_period in enumerate(contract['medical_leaves']):
                                m_leave_periods.append((4,m_period['id']))
                            # justifications
                            j_leave_periods = []
                            j_aux = 0
                            c=""
                            d=False
                            for j_array_index, j_period in enumerate(contract['justifications']):
                                j_leave_periods.append((4,j_period['id']))
                                #j_aux = j_aux + j_period['justified_minutes']
                                j_aux = j_period['justified_minutes']
                                c = j_period['comment']
                                d = j_period['justified_day']
                                nd = j_period['n_non_attendanced_day']

                            _logger.debug('DAY: ' + str(day) + " // period" + str(t_period))

                            hr_workday_movement = self.env['hr.workday.movement'].create(
                                                            {
                                                                'day_of_week': t_period['start'].strftime("%A"),
                                                                'hr_workday_process_id': self.id,
                                                                'start_time': t_period['start'],
                                                                'end_time': t_period['end'],
                                                                'closest_start': None,
                                                                'closest_end': None,
                                                                'expected_check': 0,
                                                                'hr_contract_id': id_contract,
                                                                'hr_employee_id': employee_id,
                                                                'hr_department_id': department_id,
                                                                'hr_workday_schedule_ids': n_workday_periods,
                                                                'hr_shift_schedule_ids': n_shift_periods,
                                                                'hr_leave_ids': l_leave_periods,
                                                                'hr_medical_leave_ids': m_leave_periods,
                                                                'hr_justification_ids': j_leave_periods,
                                                                'comment':c,
                                                                'justified_day':d,
                                                                'n_non_attendanced_day': nd
                                                            })

                        # _logger.debug('start: 'str(t_period['start']) + + 'leave')

        self.env.cr.commit()
        # _logger.debug(json.dumps(periods, indent=4, sort_keys=True))

class HrWorkdayMovement(models.Model):
    _name = 'hr.workday.movement'

    # _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Movimiento procesado de jornadas + turno + horas extraordinaria'


    day_of_week = fields.Char(string="Día de la semana")

    start_time = fields.Datetime(string = "Inicio procesado",track_visibility='onchange')
    end_time = fields.Datetime(string = "Fin procesado",track_visibility='onchange')


    # restriccion de fecha de inicio <= fecha de fin
    @api.constrains('start_time', 'end_time')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.start_time > r.end_time:
                raise exceptions.ValidationError("La fecha de inicio del movimiento de la jornada debe ser anterior a la fecha de fin.")


    closest_start = fields.Datetime(string = "Entrada más cercana",track_visibility='onchange')
    closest_end  = fields.Datetime(string = "Salida más cercana",track_visibility='onchange')

    hr_workday_process_id = fields.Many2one('hr.workday.process', string="Proceso de Jornadas+turnos+horas extra")

    hr_workday_schedule_ids = fields.Many2many('hr.workday.schedule', string="Jornada de referencia")
    hr_shift_schedule_ids = fields.Many2many('hr.shift.schedule', string="Turnos de referencia")
    hr_leave_ids = fields.Many2many('hr.leave', string="Permisos de referencia")

    hr_medical_leave_ids = fields.Many2many('hr.medical.leave', string="Licencias médicas de referencia")
    hr_justification_ids = fields.Many2many('hr.justification', string="Justificaciones de referencia")

    hr_contract_id = fields.Many2one('hr.contract', string="Contrato de trabajo asociado")

    hr_employee_id = fields.Many2one('hr.employee', string="Empleado asociado")
    related_employee_id = fields.Char(related="hr_employee_id.identification_id")
    hr_department_id = fields.Many2one('hr.department', string="Departamento")

    # day_of_week = fields.Char(compute='compute_day_of_week', string="Día de la semana")

    expected_check = fields.Selection(    [     (0,'No'),
                                                (1,'Si')
                                ],
                                string = 'Debió haber marcación',
                                track_visibility='onchange',
                                required = True
                            )

    difference = fields.Float(string="Diferencia en contra (Minutos)")

    #non_attendanced_day = fields.Selection(    [     (0,'No'),
    #                                            (1,'Si')
    #                            ],
    #                            string = 'Faltó ese día?',
    #                            track_visibility='onchange',
    #                            required = True
    #                        )
    non_attendanced_day = fields.Boolean(string = 'Faltó ese día?',default=False,
        track_visibility='onchange', readonly=True)
    justified_day = fields.Boolean(string = 'Se justifica esa inasistencia?', default=False,
        track_visibility='onchange')
    n_non_attendanced_day = fields.Integer(string="Inasistencia (int)", readonly=False)

    #@api.onchange('justified_day') # if these fields are changed, call method
    #def check_change(self):


    #justified_minutes = fields.Integer(string="Minutos de Justificación", compute='_justified_minutes')
    justified_minutes = fields.Integer(string="Minutos de Justificación", default=0)
    comment = fields.Char(string="Comentario de Justificación")

    @api.multi
    @api.onchange('justified_minutes', 'comment', 'justified_day') # if these fields are changed, call method
    def justified_minutes_change(self):
        # CREAR SI NO EXISTE
        #### PENDIENTE
        #EXISTE
        import logging
        _logger = logging.getLogger(__name__)
        _logger.debug("\n---justified_minutes: " + str(self.justified_minutes))
        _logger.debug("\n--2--hr_justification_ids: " + str(self.hr_justification_ids))
        if self.justified_day:
            self.n_non_attendanced_day = 0
            self.justified_minutes = 0
        else:
            self.n_non_attendanced_day = 1 

        if(len(self.hr_justification_ids)==0):
            from datetime import date, datetime
            jm = self.env['hr.justification'].create([{
                'justified_minutes': self.justified_minutes,
                'comment':self.comment,
                'justified_day': self.justified_day,
                'date': str(datetime.strptime(str(self.start_time), "%Y-%m-%d %H:%M:%S").date()),
                'n_non_attendanced_day': self.n_non_attendanced_day,
                #'date': "2019-09-02",
                'employee_id': self.hr_employee_id.id}])
            self.update({'hr_justification_ids': [(4, jm.id)] })
        else:
            #jm = self.env['hr.justification'].create([{'justified_minutes': self.justified_minutes}])
            self.update({'hr_justification_ids': [(1, self.hr_justification_ids.id, {
                'justified_minutes': self.justified_minutes, 'comment': self.comment, 'justified_day': self.justified_day, 'n_non_attendanced_day': self.n_non_attendanced_day})] })

        #jm = self.env['hr.justification'].search([
        #        ('office', '=', self.id),
        #        ], order='timestamp desc', limit = 1)
        #if len(last_mark)>0:
        #    last_mark = last_mark[0].timestamp
                
        #ping = self.env['ping'].create([{'office' : id_clock,'timestamp': this_time,'ping': 1}])
        #self.write({'ping_status': [(4, ping.id)] })

        #self.write({'hr_justification_ids': [(0,0,self.justified_minutes)] })
        #self.write({'hr_justification_ids': [(0,0,{'justified_minutes': 500})] })
        self.env.cr.commit()
        _logger.debug("\n---self.hr_justification_ids: " + str(self.hr_justification_ids))
        #self.hr_justification_ids.write({'hr_justification_ids': [(1, self.hr_justification_ids.id, self.justified_minutes)] })
        #self.write({'hr_justification_ids': [(1, self.hr_justification_ids.id, self.justified_minutes)] })
        #self.env.cr.commit()

    #@api.one
    #@api.constrains('justified_minutes')
    #def _justified_minutes(self):
    #        if self.justified_minutes > self.difference:
    #            self.justified_minutes = self.difference
    #        elif self.justified_minutes < 0:
    #            self.justified_minutes = 0

    #@api.one
    #@api.depends('justified_minutes')
    #def _justified_minutes(self):
    #        if self.justified_minutes > self.difference:
    #            self.justified_minutes = self.difference
    #        elif self.justified_minutes < 0:
    #            self.justified_minutes = 0

    # @api.depends('start')
    # def compute_day_of_week(self):
    #     if self.start != False:
    #         import datetime
    #         self.day_of_week = self.start.strftime('%A')
    #     else:
    #         self.day_of_week  = False

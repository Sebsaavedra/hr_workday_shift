# -*- coding: utf-8 -*-
from datetime import datetime, timedelta



from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT


#class AttendanceRecapReportWizard(models.TransientModel):
#    _name = 'attendance.recap.report.wizard'

class ClockRecapReportWizard(models.TransientModel):
    _name = 'hr_workday_shift.recap.report.wizard'
    date_start = fields.Date(string="Fecha Inicial", required=True, default=fields.Date.today)
    date_end = fields.Date(string="Fecha Final", required=True, default=fields.Date.today)
    #clock = fields.Many2one('clock', string='Reloj', required=True)
    clock = fields.Many2one('hr.department', string='Reloj')
    employee = fields.Many2one("hr.employee", "Empleado")

    @api.multi
    def get_report(self):
        """Call when button 'Get Report' clicked.
        """
        data = {
            'ids': self.ids,
            'model': self._name,
            'form': {
                'date_start': self.date_start,
                'date_end': self.date_end,
                'clock': self.clock.id,
                'employee': self.employee.id,
            },
        }

        # use `module_name.report_id` as reference.
        # `report_action()` will call `_get_report_values()` and pass `data` automatically.
        return self.env.ref('hr_workday_shift.recap_report').report_action(self, data=data)



#class ReportAttendanceRecap(models.AbstractModel):
class ReportClockRecap(models.AbstractModel):
    """Abstract Model for report template.

    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """
    _name = 'report.hr_workday_shift.clock_recap_report_view'

    @api.model
    def _get_report_values(self, docids, data=None):
        date_start = data['form']['date_start']
        clock = data['form']['clock']
        date_end = data['form']['date_end']
        employee = data['form']['employee']

        date_start_obj = datetime.strptime(date_start, DATE_FORMAT)
        date_end_obj = datetime.strptime(date_end, DATE_FORMAT)
        date_diff = (date_end_obj - date_start_obj).days + 1

        docs = []

        #clock_name = "Todos los relojes"
        #return {
        #    'doc_ids': data['ids'],
        #    'doc_model': data['model'],
        #    'date_start': date_start,
        #    'date_end': date_end,
        #    'clock_name': clock_name,
        #    'docs': docs,
        #}
        
                        
        #employees = self.env['hr.employee'].search([], order='name asc')
        t2 = (date_end_obj + timedelta(days=1))

        search_criteria = [
                ('punch', '>=', date_start_obj.strftime(DATETIME_FORMAT)),
                ('punch', '<', t2.strftime(DATETIME_FORMAT)),
        ]
        clock_name = "Todos los relojes"
        if clock:
            clock_name =  self.env['hr.department'].search(
            [['id', '=',  clock]],
            limit = 1
            )[0]["name"]
            search_criteria.append(('hr_department_id', '=',  clock))
        
        if employee:
            tupla = ('identification_id', '=', employee)
            search_criteria.append(tupla)

        marks = self.env['hr.clock.punch'].search(search_criteria, order='punch asc')
        
        import logging
        _logger = logging.getLogger(__name__)
        _logger.debug("\n\n+++ [search_criteria]: " + str(search_criteria) + "\n\n")

        for mark in marks:
            employee_name =  self.env['hr.employee'].search(
            [['identification_id', '=',  mark.identification_id]],
            limit = 1
            )
            if(len(employee_name) > 0):
                employee_name = employee_name[0]["name"]
            else:
                employee_name = "???"
            docs.append({
                'employee': employee_name,
                'employee_id': mark.identification_id,
                'marca': mark.punch,
                #'punch': "Entrada" if mark.punch==0 else "Salida"
                #'hr_department_id': department.name,
                'hr_department_id': clock_name,
                'REF': clock_name,
            })
        n_registers = len(marks)

        #n_employees = len(self.env['hr.clock.punch'].search(search_criteria, order='punch asc'))
        n_employees = len(self.env['hr.clock.punch'].read_group(search_criteria, fields=['id'], groupby=['identification_id']))
        #result = self.env['yourmodel'].read_group([ ("type", "=", "product") ], fields=['Id'], groupby=['Id'])
        today = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'date_start': date_start,
            'date_end': date_end,
            'clock_name': clock_name,
            'docs': docs,
            'n_registers': n_registers,
            'n_employees': n_employees,
            'today': today,
        }
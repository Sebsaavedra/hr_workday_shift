# -*- coding: utf-8 -*-

from odoo.exceptions import Warning
from odoo import models, fields, api, _


class HrDepartmentInherit(models.Model):
    ip = fields.One2many('hr.medical.leave', 'employee_id', string="Licencias médicas")
    _inherit = 'hr.department'


    clock_ip = fields.Char('IP')
    clock_port = fields.Char('Puerto')
    sensitive_field = fields.Char('Pass (no cambiar)', default="0")
    #input_file = fields.Char('Archivo temporal', default='/home/ricardo/Escritorio/mytemp/'+self.id+'attendances.csv')
    #department_id


    clock_status = fields.Boolean('ESTADO', readonly=True)
    clock_updated_at = fields.Datetime('Última conexión', readonly=True)

    clock_n_users = fields.Integer("Usuarios", readonly=True)
    clock_n_users_cap = fields.Integer("Capacidad", readonly=True)
    clock_n_fingers = fields.Integer("Huellas", readonly=True)
    clock_n_fingers_cap = fields.Integer("Capacidad", readonly=True)
    clock_n_records = fields.Integer("Registros", readonly=True)
    clock_n_rec_cap = fields.Integer("Capacidad", readonly=True)

    hr_clock_punch_ids = fields.One2many('hr.clock.punch', 'hr_department_id', string="Marcaciones de RC", track_visibility='onchange')


    def string2utc(self, t):
        import pytz
        from datetime import datetime
        local = pytz.timezone ("Chile/Continental")
        naive = datetime.strptime (t, "%Y-%m-%d %H:%M:%S")
        local_dt = local.localize(naive, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        return utc_dt

    @api.one
    def button_ping(self,cr):

        import csv
        import time
        from datetime import datetime # fijar hora
        import xmlrpc.client
        import pprint
        from datetime import timedelta
        import pytz
        import threading
        import os
        import pandas as pd
        import shutil
        from .zk import ZK
        from .zk.const import USER_ADMIN
        import pytz
        import traceback

        import logging
        _logger = logging.getLogger(__name__)

        conn = None
        #zk = ZK("192.168.10.90", port=4370, timeout=5, password=0, force_udp=False, ommit_ping=False)
        zk = ZK(self.clock_ip, port=int(self.clock_port), timeout=20, password=self.sensitive_field, force_udp=False, ommit_ping=False)
        try:
            n_users0 = self.clock_n_users
            n_records0 = self.clock_n_records
            _logger.debug("\n\nEntra el try")
            # BORRAR EL IF
            if(True):
                # connect to device
                conn = zk.connect()
                #conn.set_time(newtime)
                conn.read_sizes()
                _logger.debug("\n\nAQUI")
                self.clock_n_users = int(conn.users)
                self.clock_n_users_cap = int(conn.users_cap)
                self.clock_n_fingers = int(conn.fingers)
                self.clock_n_fingers_cap = int(conn.fingers_cap)
                self.clock_n_records = int(conn.records)
                self.clock_n_rec_cap = int(conn.rec_cap)

                self.clock_status = True
                _logger.debug("\n\nAQUI2")
            if(True):

                ##self.create_employee(name, identification_id)
                ##listen_live_capture()
                ##name = "NUEVO"
                ##identification_id = 12341234
                ##self.env['hr.employee'].create({'name': name,'identification_id': identification_id})
                ##self.env.cr.commit()
                ##conn.test_voice()
                # department_id = self.id
                # id_clock = self.id
                this_time = datetime.now()#self.string2utc(time1)
                # ping = self.env['ping'].create([{'office' : id_clock,'timestamp': this_time,'ping': 1}])
                # self.write({'ping_status': [(4, ping.id)] })
                # self.env.cr.commit()
                ##employee = self.create_employee(name, identification_id)
                ##att_obj = self.env['hr.attendance']
                ##employee.write({'clocks_ids': [(4, self.id, employee.id)]})
                s_timestamp = (time.strftime("%Y-%m-%d %H:%M"))
                path = '/tmp/'
                #file_users = "users"
                file_attendances = "attendances"

                ## Check
                _logger.debug("\n\nAQUI3")
                previous = str(self.clock_updated_at)
                _logger.debug("\n\nAQUI4")
                input_file = path + "clock_" + str(self.id) + ".csv"
                #input_file = path+str(department_id)+';'+str(s_timestamp)+';'+file_attendances+'.csv'


                ###if(self.n_records != n_records0):

                try:
                    _logger.debug("\n\nEntra a creación del archivo")
                    with open(input_file , 'w', newline='') as csvfile:
                        attendances = conn.get_attendance()
                        spamwriter = csv.writer(csvfile, delimiter=';',quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        spamwriter.writerow(['user_id', 'timestamp', 'status', 'punch', 'uid'])
                        for user in attendances:
                            #spamwriter.writerow([user.uid, user.name, privilege, user.user_id])
                            # Attendance(user_id, timestamp, status, punch, uid)
                            spamwriter.writerow([user.user_id, user.timestamp, user.status, user.punch, user.uid])
                        ##### BORRAR ->
                        #spamwriter.writerow([previous, self.n_records, n_records0]) # BORRAR
                        ##### BORRAR <-
                        """
                        loop csv file beginning specific row
                        https://www.google.com/search?client=ubuntu&channel=fs&q=loop+csv+file+beginning+specific+row&ie=utf-8&oe=utf-8
                        """

                except Exception as e:
                    _logger.debug("\n\n ERROR: "+str(e))

                #####input_file = path+str(department_id)+';'+str(s_timestamp)+';'+file_attendances+'.csv'
                ##### DESCARGAR MARCACIONES
                ##### DESCARGAR EMPLEADOS

                ####clock_file = path+"reloj.csv" # IDreloj.csv
                ####input_file = path+"mini.csv" # IDreloj-timestamp.csv # path+str(department_id)+"_"+file_attendances+'.csv'
                #####input_file = path+str(department_id)+';'+str(s_timestamp)+';'+file_attendances+'.csv'

                delta_lines = pd.read_csv(input_file, sep=";")
                # DESCOMENTAR
                last_mark = self.env['hr.clock.punch'].search([
                ('hr_department_id', '=', self.id),
                ], order='punch desc', limit = 1)
                if len(last_mark)>0:
                    last_mark = last_mark[0].punch
                    #last_mark = "10/09/2019 09:25:11"
                    delta_lines = delta_lines[pd.to_datetime(delta_lines["timestamp"])  > pd.to_datetime(last_mark) ]


                #if( len(delta_lines.index) > 0 ):
                # _logger.debug("E_")
                j = 0
                if( len(delta_lines) > 0 ):

                    # _logger.debug("E_0")
                    for index, row in delta_lines.iterrows():

                        # _logger.debug("E_1")
                        self.env['hr.clock.punch'].create({
                            'internal_clock_id': row['uid'],
                            'hr_department_id' : self.id,
                            'identification_id' : row['user_id'],#4,#(get_id_employee(identification_id)).id, ## CAMBIAR A USER
                            'punch': self.string2utc(row['timestamp']),
                            'original_punch': row['timestamp']
                        })
                        # _logger.debug("E_2")
                        #agrego el empleado al reloj
                        #new_punch.write({'punch_ids': [(4, id_clock, new_punch.id)]})
                        if j%1000==0:
                            _logger.debug(str("{:,}".format(j)) + "rows inserted")

                        j = j+1

                        self.env.cr.commit()
                traceback.print_exc()


        except Exception as e:
            print(e)
            _logger.debug("\n\n ERROR GENERAL: "+str(e))
            # _logger.debug("\n\n " + str(err))
            self.clock_status = False
            self.clock_n_users = 0
            self.clock_n_users_cap = 0
            self.clock_n_fingers = 0
            self.clock_n_fingers_cap = 0
            self.clock_n_records = 0
            self.clock_n_rec_cap = 0

            #
            # id_clock = self.id
            # this_time = datetime.now()#self.string2utc(time1)
            # ping = self.env['ping'].create([{'office' : id_clock,'timestamp': this_time,'ping': 0}])
            # self.write({'ping_status': [(4, ping.id)] })
            # self.env.cr.commit()
            #raise Exception("\n** Except en <button_ping> . --\n",e,"\n")
            print ("Process terminate : {}".format(e))

        finally:
            self.clock_updated_at = datetime.now()
            if conn:
                # conn.end_live_capture = True
                conn.enable_device()
                conn.disconnect()
        # print("\nFIN")



class HrClockPunch(models.Model):

    _name = 'hr.clock.punch'
    _description = 'Registros de marcación'

    identification_id = fields.Char('N° de indentificación')
    internal_clock_id = fields.Char('ID interno en el reloj')
    punch = fields.Datetime(string="Fecha desde", required=True)
    original_punch = fields.Datetime(string="Fecha desde original", required=True)

    hr_department_id = fields.Many2one('hr.department', string="Departamento asociado")

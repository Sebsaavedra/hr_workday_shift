# -*- coding: utf-8 -*-
{
    'name': "Jornadas y turnos de empleados",
    'version': '0.0.0.1',
    'summary': """Jornadas, turnos y horas extraordinarias en corporación municipal de derecho privado""",
    'description': """ Permite el manejo de jornadas laborales para distintos tipos
                    de contratos. También, ciclos de aprobación de de turnos y horas extraordinarias,
                    y vistas consolidadas junto a jornadas. Útles para corporaciones de salud y educación en chile.""",
    'category': 'Empleados',
    'author': 'Boris De los Ríos',
    'company': '',
    'maintainer': 'Boris De los Ríos',
    'website': "",
    'depends': ['hr', 'resource'],
    'data': [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/hr_employee_workday_view.xml',
        'views/hr_employee_contract_view.xml',
        'views/hr_department_view.xml',
        'views/custom_report_template.xml', # vista para wizard de reporte en pdf
        'views/custom_report_wizard.xml',   # vista para wizard de reporte en pdf
        'views/templates.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': True,
}
